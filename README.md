This document was pillaged from https://medium.com/@rands/how-to-rands-c26f81e09f02

# How to Andrew

Hi, welcome to the team. I’m so glad you are here at GitLab.

It’s going to take a solid quarter to figure this place out. I understand the importance of first impressions, and I know you want to get a check in the win column, but this is a complex place full of equally complex humans. Take your time, meet everyone, go to every meeting, write things down, and ask all the questions — especially about all those baffling acronyms and emoji.

One of the working relationships we need to define is ours. The following is a user guide for me and how I work. It captures what you can expect out of the average week, how I like to work, my north star principles, and some of my, uh, nuance. My intent is to accelerate our working relationship with this document.

## Our Average Week

We’ll have a 1:1 every week for at least 30 minutes no matter what. This meeting discusses topics of substance, not updates. I’ve created a private Slack channel for the two us of to capture future topics for our 1:1s. When you or I think of a topic, we dump it in that channel.

We’ll have a staff meeting with your peers every week for 30 minutes. Unlike 1:1s, we have a shared document which captures agenda topics for the entire team. Similar to 1:1s, we aren’t discussing status at this meeting, but issues of substance that affect the whole team.

You can Slack me 24 hours a day. 

If I am traveling, I will give you notice of said travel in advance. All our meetings still occur albeit with time zone considerations.

I try not to work on the weekends as I have a young family. This is my choice and you have the same. I do not expect that you are going to work on the weekend. 

I might Slack you things, but unless the thing says URGENT, it can always wait until work begins for you on Monday.

## North Star Principles

## Feedback Protocol

I firmly believe that feedback is at the core of building trust and respect in a team.

At GitLab, there is a formal feedback cycle which occurs twice a year. The first time we go through this cycle, I’ll draft a proposed set of goals for you for the next review period. These are not product or technology goals; these are professional growth goals for you. I’ll send you these draft goals as well as upward feedback from your team before we meet so you can review beforehand.

In our face-to-face meeting, we’ll discuss and agree on your goals for the next period, and I’ll ask for feedback on my performance. At our following review, the process differs thusly: I’ll review you against our prior goals, and I’ll introduce new goals (if necessary). Rinse and repeat.

Review periods are not the only time we’ll exchange feedback. This will be a recurring topic in our 1:1s. I am going to ask you for feedback in 1:1s regularly. I am never going to stop doing this no matter how many times you say you have no feedback for me.

Disagreement is feedback and the sooner we learn how to efficiently disagree with each other, the sooner we’ll trust and respect each other more. Ideas don’t get better with agreement.

## Meeting Protocol

I attend a lot of meetings. I deliberately run with my calendar publicly visible. If you have a question about a meeting on my calendar, ask me. If a meeting is private or confidential, it’s title and attendees will be hidden from your view. The vast majority of my meetings are neither private nor confidential.

Please feel free to reschedule 1:1 meetings. There's no need to consult with me first unless it overlaps with another calendar item or is outside of my office hours.

My definition of a meeting includes an agenda and/or intended purpose, the appropriate amount of productive attendees, and a responsible party running the meeting to a schedule. If I am attending a meeting, I’d prefer starting on time. If I am running a meeting, I will start that meeting on time.

If a meeting completes its intended purpose before it’s scheduled to end, let’s give the time back to everyone. If it’s clear the intended goal won’t be achieved in the allotted time, let’s stop the meeting before time is up and determine how to finish the meeting later.

## Nuance and Errata

* I'm an engineer and I love detail. If you feel that I'm going too in-depth, feel free to tell me


This document is a living breathing thing and likely incomplete. I will update it frequently and would appreciate your feedback.